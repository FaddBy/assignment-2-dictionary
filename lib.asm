global _start
section .data
char: db 0, 0
alf:  db '0123456789'
minus: db '-'
section .text
global exit, string_length, print_string, print_char, print_newline, print_uint, print_int, string_equals, read_char, read_word, parse_uint, parse_int, string_copy
; dt Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; dt Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; dt Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; dt Принимает код символа и выводит его в stdout
print_char:
    mov rax, 1
    mov byte[char], dil
    mov rdi, 1
    mov rsi, char
    mov rdx, 1
    syscall
    ret

; dt Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; dt беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rcx, rcx
    xor rax, rax
.floop:
    inc rcx
    mov rax, rdi
    xor rdx, rdx
    mov r8, 10
    idiv r8
    mov rdi, rax
    push rdx
    test rax, rax
    jnz .floop
.sloop:
    dec rcx
    pop rax
    mov rdi, [alf + eax]
    push rcx
    call print_char
    pop rcx
    test rcx, rcx
    jnz .sloop
    ret

; dt Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rcx, rcx
    xor rax, rax
    cmp rdi, 0
    jge .floop
    push rdi
    mov rdi, [minus]
    call print_char
    pop rdi
    neg rdi
    xor rcx, rcx
.floop:
    inc rcx
    mov rax, rdi
    xor rdx, rdx
    mov r8, 10
    idiv r8
    mov rdi, rax
    push rdx
    test rax, rax
    jnz .floop
.sloop:
    dec rcx
    pop rax
    mov rdi, [alf + eax]
    push rcx
    call print_char
    pop rcx
    test rcx, rcx
    jnz .sloop
    ret
    
; dt Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor rax, rax
.loop:
    mov al, [rdi + rcx]
    mov dl, [rsi + rcx]
    test al, al
    jz .end
    test dl, dl
    jz .end
    cmp al, dl
    jne .zero
    inc rcx
    jmp .loop
.end:
    cmp al, dl
    jne .zero
.one:
    mov rax, 1
    ret
.zero:
    mov rax, 0
    ret
; dt Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    push rcx
    syscall 
    pop rcx
    test rax, rax
    jz .end
    mov al, [rsp]
.end:
    inc rsp
    ret

; d Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
.spaceloop:
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    test rax, rax
    jz .end
    cmp al, 0x20
    je .space
    cmp al, 0x9
    je .space
    cmp al, 0xA
    je .space
    jmp .loop
.space:
    inc rcx
    cmp rsi, rcx
    jl .fail
    jmp .spaceloop
.loop:
    mov byte[rdi + r8], al
    inc r8
    add rcx, r8
    cmp rsi, rcx
    jl .fail
    sub rcx, r8 
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    test al, al
    jz .end
    cmp al, 0x20
    je .end
    cmp al, 0x9
    je .end
    cmp al, 0xA
    je .end
    jmp .loop
.end:
    mov byte[rdi + r8], 0
    mov rdx, r8
    mov rax, rdi
    ret
.fail:
    mov rax, 0
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    xor rsi, rsi
.loop:
    mov al, [rdi + rcx]
    push rdi
    mov rdi, rax
    call check_int
    test rax, rax
    jz .end
    imul rsi, 10
    mov rax, rdi
    pop rdi
    sub rax, 48
    add rsi, rax
    inc rcx
    jmp .loop
.end:
    pop rdi
    test rcx, rcx
    jz .fail
    mov rdx, rcx
    mov rax, rsi
    ret
.fail:
    mov rdx, 0
    ret

check_int:
    cmp rdi, '0'
    je .yes
    cmp rdi, '1'
    je .yes
    cmp rdi, '2'
    je .yes
    cmp rdi, '3'
    je .yes
    cmp rdi, '4'
    je .yes
    cmp rdi, '5'
    je .yes
    cmp rdi, '6'
    je .yes
    cmp rdi, '7'
    je .yes
    cmp rdi, '8'
    je .yes
    cmp rdi, '9'
    je .yes
.no:
    mov rax, 0
    ret
.yes:
    mov rax, 1
    ret
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov al, [rdi]
    cmp al, '-'
    je .yes
    jmp .no
.yes:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 
.no:
    jmp parse_uint
    ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
.loop:
    mov al, [rdi + rcx]
    test al, al
    jz .yes
    mov byte[rsi + rcx], al
    inc rcx
    cmp rdx, rcx
    jle .no
    jmp .loop
.yes:
    mov byte[rsi + rcx], 0
    mov rax, rcx
    ret
.no:
    mov rax, 0
    ret
