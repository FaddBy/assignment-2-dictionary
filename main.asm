%include "words.inc"
%include "lib.inc"

%define BUFFER_SIZE 255
%define POINTER_SIZE 8
%define STDOUT 1
%define STDERR 2

section .data
    buffer: times BUFFER_SIZE db 0x0

section .rodata
    input_err_msg: db "Error - string is too long", 0
    not_found_err_msg: db "Error - key is not found", 0
    first: db "firstword", 0

section .text
extern find_word
global _start

print_error:
    call string_length
    
    mov rdx, rax
    mov rax, STDOUT
    mov rsi, rdi
    mov rdi, STDERR
    syscall
    
    jmp print_newline
    ret


_start:
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_word
    test rax, rax
    je .input_error 
    push rdx
    mov rdi, buffer
    mov rdi, buffer
    mov rsi, first_word
    call find_word
    test rax, rax
    je .find_error

    pop rdx
    lea rdi, [rax + rdx + POINTER_SIZE + 1]
    call print_string
    call print_newline
    jmp .exit
    
    .input_error:
        mov rdi, input_err_msg
        call print_error
        jmp .exit

    .find_error:
	    mov rdi, not_found_err_msg
        call print_error
    
    .exit:
        call exit