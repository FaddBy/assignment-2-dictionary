ASM=nasm
FLAGS=-f elf64
LD = ld

.PHONY: clean

prog: main.o lib.o dict.o
	$(LD) -o $@ $^

lib.o: lib.asm
	$(ASM) $(FLAGS) -o $@ $^

main.o: main.asm
	$(ASM) $(FLAGS) -o $@ $^

dict.o: dict.asm
	$(ASM) $(FLAGS) -o $@ $^
        
clean:
	rm *.o
