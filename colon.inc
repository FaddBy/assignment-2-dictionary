%macro colon 2
; %1 - key
; %2 - label

%define label %2

	%ifdef prev
		label: dq prev
	%else
		label: dq 0
	%endif
	db %1, 0x0
	%xdefine prev label

%endmacro
