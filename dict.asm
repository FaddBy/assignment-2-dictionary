%include "lib.inc"

%define POINTER_SIZE 8

section .text
global find_word

find_word:
    ; rdi - слово
    ; rsi - текущий элемент словаря
    .loop:
        push rsi
        add rsi, POINTER_SIZE
        call string_equals
        pop rsi
        test rax, rax
        jnz .found
        mov rsi, [rsi]
        test rsi, rsi
        jz .not_found
        jmp .loop
        mov rax, rsi
        ret
    .found:
        mov rax, rsi
        ret
    .not_found:
        xor rax, rax
        ret
